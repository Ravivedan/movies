package com.example.movies.utils.data

data class MovieResult(
    val page: Int,
    val results: ArrayList<MovieData>,
    val total_pages: Int,
    val total_results: Int
)

data class MovieData(
    val id: String, val original_language: String, val popularity: String, val title: String
)
