package com.example.movies.utils

import com.example.movies.utils.data.MovieResult
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ApiInterface {
    // https://api.themoviedb.org/3/movie/top_rated?api_key=ec01f8c2eb6ac402f2ca026dc2d9b8fd
    /* @GET("3/movie/top_rated?api_key=ec01f8c2eb6ac402f2ca026dc2d9b8fd")
     fun listOfMovies(): Call<MovieResult>
 */
    @GET("3/movie/top_rated?api_key=ec01f8c2eb6ac402f2ca026dc2d9b8fd")
    fun listOfMovies(@Query("page") page: Int): Call<MovieResult>?

    /* @GET("users/{username}")
    Call<User> getUser(@Path("username") String username);

    @GET("group/{id}/users")
    Call<List<User>> groupList(@Path("id") int groupId, @Query("sort") String sort);

*/

    companion object {

        var BASE_URL = "https://api.themoviedb.org/"

        fun create(): ApiInterface {

            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
                .readTimeout(80, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(httpClient.build())
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}