package com.example.movies.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movies.R
import com.example.movies.utils.data.MovieData
import kotlinx.android.synthetic.main.row_item.view.*

class MovieAdapter(val context: Context, val clickListener: ItemClickListener) :
    RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    val movieList = mutableListOf<MovieData>()

    fun addDataMore(movie: ArrayList<MovieData>) {
        movieList.addAll(movie)
        notifyDataSetChanged()
    }

    fun addData(movie: ArrayList<MovieData>) {
        movieList.clear()
        movieList.addAll(movie)
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun clickItem(movieData: MovieData)
    }

    inner class MovieViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindUI(position: Int) {
            view.apply {
                movieList[position].let {
                    idTextTv.text = it.id
                    originalLanguageTv.text = it.original_language
                    popularityTv.text = it.popularity
                    idTitleTv.text = it.title
                    rowLL.setOnClickListener {
                        clickListener.clickItem(movieList[position])
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieAdapter.MovieViewHolder = MovieViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row_item, parent, false)
    )

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindUI(position)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }
}