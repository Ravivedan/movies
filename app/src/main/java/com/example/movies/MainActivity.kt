package com.example.movies

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movies.adapter.MovieAdapter
import com.example.movies.utils.*
import com.example.movies.utils.data.MovieData
import com.example.movies.utils.data.MovieResult
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.DialogInterface
import android.view.View

/*
* Jan-19-2022
* Created by ravikumar.m
* raviashra@gmail.com
* Internet connection checked
* Api call available
* simple load more concept implemented
* Exit dialog is implemented in onBackPress()
* */

class MainActivity : AppCompatActivity(), MovieAdapter.ItemClickListener {

    var pageNumber: Int = 1
    lateinit var movieAdapter: MovieAdapter

    override fun onStart() {
        super.onStart()
        callApi()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val layoutManager = LinearLayoutManager(this)
        recyclerView.setLayoutManager(layoutManager)
        movieAdapter = MovieAdapter(this, this)
        recyclerView.adapter = movieAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!recyclerView.canScrollVertically(1)) {
                    // load more api call
                    callApi()
                }
            }
        })
        refreshBt.setOnClickListener {
            refreshBt.visibility = View.GONE
            noDataAvailableTV.visibility = View.GONE
            callApi()
        }
    }

    private fun callApi() {

        when (isNetworkAvailable(this)) {
            true -> {
                llBackground.blockUI(this)
                val apiInterface = ApiInterface.create().listOfMovies(pageNumber)
                apiInterface!!.enqueue(object : Callback<MovieResult> {
                    override fun onResponse(
                        call: Call<MovieResult>?,
                        response: Response<MovieResult>?
                    ) {
                        if (response!!.isSuccessful) {
                            val res: MovieResult = response.body()!!
                            if (movieAdapter.movieList.size == 0) {
                                movieAdapter.addData(res.results)
                            } else {
                                movieAdapter.addDataMore(res.results)
                            }

                            if (movieAdapter.movieList.size == 0) {
                                pageNumber = res.page
                                refreshBt.visibility = View.VISIBLE
                                noDataAvailableTV.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            } else {
                                if (response.body()!!.results.size == 0) {
                                    toast(resources.getString(R.string.no_more_data_available))
                                } else {
                                    refreshBt.visibility = View.GONE
                                    noDataAvailableTV.visibility = View.GONE
                                    recyclerView.visibility = View.VISIBLE
                                    pageNumber = res.page + 1
                                }
                            }
                            llBackground.unBlockUI(this@MainActivity)
                        } else {
                            llBackground.unBlockUI(this@MainActivity)
                            toast(resources.getString(R.string.please_try_after_some_time))
                        }
                    }

                    override fun onFailure(call: Call<MovieResult>, t: Throwable) {
                        llBackground.unBlockUI(this@MainActivity)
                        toast(resources.getString(R.string.please_try_after_some_time))
                    }
                })
            }
            else -> {
                llBackground.unBlockUI(this@MainActivity)
                toast(resources.getString(R.string.check_your_internet_connection))
                if (movieAdapter.movieList.size == 0) {
                    refreshBt.visibility = View.VISIBLE
                    noDataAvailableTV.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                } else {
                    refreshBt.visibility = View.GONE
                    noDataAvailableTV.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun clickItem(movieData: MovieData) {
        Toast.makeText(this, "" + movieData.title, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(resources.getString(R.string.exit))
            .setMessage(resources.getString(R.string.check_your_internet_connection))
            .setPositiveButton(resources.getString(R.string.yes),
                DialogInterface.OnClickListener { dialog, which -> finish() })
            .setNegativeButton(resources.getString(R.string.no), null)
            .show()
    }
}